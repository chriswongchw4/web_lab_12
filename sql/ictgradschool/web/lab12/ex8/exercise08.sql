-- Answers to Exercise 8 here

-- delete a row from one table
DELETE FROM lab12ex06_customerdb
WHERE id=3;


-- delete a column from the same table
ALTER TABLE lab12ex07_article DROP title;


-- delete entire table
DROP TABLE lab12ex07_comments;


-- change a string value
UPDATE lab12ex07_article
SET title='changeString'
WHERE id='2';

-- change a value that is a numeric value
UPDATE lab12ex06_movie
SET weeklychrage='2' -- orginal is 6, now changed to 2
WHERE id='2';

-- change a value that is a primary key
UPDATE lab12ex07_comments
SET id='5'
WHERE article_id=1;