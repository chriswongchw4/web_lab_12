-- Answers to Exercise 5 here

DROP TABLE IF EXISTS lab12ex05_user;

CREATE TABLE IF NOT EXISTS lab12ex05_user (
    username VARCHAR(15) NOT NULL,
    first_name VARCHAR(20),
    last_name VARCHAR(10),
    email VARCHAR(40),
    PRIMARY KEY (username)
);

INSERT INTO lab12ex05_user VALUES
    ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
    ('user1', 'Peter', 'Pen', 'peterpen@waikato.co.nz'),
    ('user2', 'Pete', 'Pencil', 'petepenci@gmail.com'),
    ('user3', 'OMG', 'Peterson', 'omgpeterson@omg.com')
;

SELECT * FROM lab12ex05_user