-- Answers to Exercise 2 here

DROP TABLE IF EXISTS dbtest_json;

CREATE TABLE IF NOT EXISTS dbtest_json (
    username VARCHAR(15) NOT NULL,
    first_name VARCHAR(20),
    last_name VARCHAR(10),
    email VARCHAR(40),
    PRIMARY KEY (username)
);

INSERT INTO dbtest_json VALUES
    ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
    ('user1', 'Peter', 'Pen', 'peterpen@waikato.co.nz'),
    ('user2', 'Pete', 'Pencil', 'petepenci@gmail.com'),
    ('user3', 'OMG', 'Peterson', 'omgpeterson@omg.com');