-- Answers to Exercise 9 here

-- All information about all the members of the video store
SELECT * FROM lab12ex06_customerdb;

-- Everything about all the members of the video store, except the number of video hires they have
SELECT id,name,gender,year_born,joined
FROM lab12ex06_customerdb;

-- All the titles to the articles that have been written
SELECT title FROM lab12ex07_article
WHERE id IN(SELECT article_id FROM lab12ex07_comments);
/*INNER JOIN lab12ex07_comments ON lab12ex07_article.id = lab12ex07_comments.article_id; -- title test4 havent been written as no comments.*/

-- All the directors of the movies the video store has (without repeating any names).
SELECT DISTINCT director FROM lab12ex06_movie;

-- All the video titles that rent for $2 or less a week.
SELECT title FROM lab12ex06_movie
WHERE weeklychrage<=2; -- no weeklycharge less than 2 so us less than or equal to.

-- A sorted list of all the usernames that have been registered.
SELECT username FROM lab12ex05_user
ORDER BY username ASC ;

-- All the usernames where the user’s first name starts with the letters ‘Pete’.
SELECT username FROM lab12ex05_user
WHERE first_name like 'pete%';


-- All the usernames where the user’s first name or last name starts with the letters 'Pete'
SELECT username FROM lab12ex05_user
WHERE first_name like 'pete%' OR last_name like 'pete%';